﻿using System;

namespace Assets.RTSCore.Information
{
	[Serializable]
    public class UnitInformation : Information
    {
        public float WalkSpeed;
        public float RunSpeed;
    }
}
