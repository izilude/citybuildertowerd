using System;
using System.Collections.Generic;

namespace Assets.RTSCore.Level
{
	[Serializable]
	public class ItemInformation
	{
		public string ItemName;
		public List<ItemConversion> Conversions;
	}
}

