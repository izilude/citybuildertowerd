using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.RTSCore.Level
{
	[Serializable]
	public class ItemManager : MonoBehaviour
	{
		public List<ItemInformation> ItemList;
	}
}
